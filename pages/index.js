import Link from "next/link";
import Layout from "../components/Layout";

const Index = () => (
  <Layout title="Home">
    <Link href="/about">
      <a>Goto About Page</a>
    </Link>
    <div>This is Next Js Page</div>
  </Layout>
);

export default Index;
