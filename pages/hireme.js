import Layout from "../components/Layout";

const HireMe = () => (
  <Layout title="Hire Me">
    <p>
      you can hire me at {""}
      <a href="mailto:vadivel.chandran@usbank.com">
        vadivel.chandran@usbank.com
      </a>
    </p>
  </Layout>
);

export default HireMe;