import Link from "next/link";
import Layout from "../components/Layout";
import { Component } from "react";
import fetch from "isomorphic-unfetch";
import Error from "./_error";
export default class About extends Component {

  static async getInitialProps() {
    const res = await fetch("https://api.github.com/users/dds");
    const statusCode = res.status > 200 ? res.status : false;
    const data = await res.json();
    return { user: data, statusCode };
  }

  render() {
    const { user, statusCode } = this.props;
    if (statusCode) {
      return <Error statusCode={statusCode} />
    }

    return (
      <Layout title="About" >
        <Link href="/index">
          <a>Goto Home</a>
        </Link>

        <div>{user.name}</div>
        <img src={user.avatar_url} alt="My Image" />
      </Layout>
    );
  }

}

// const About = () => (
//   <Layout title="About">
//     <Link href="/index">
//       <a>Goto Home</a>
//     </Link>
//     <div>This is About Page</div>
//     <img src="static/images/jslogo.png" alt="Javascript Image" />
//   </Layout>
// );
//export default About;
