import Layout from "../components/Layout";
import Link from "next/link";

const PostLik = ({ slug, title }) => (
    <Link as={`${slug}`} href={`./post?title=${title}`}>
        <a>{title}</a>
    </Link>

);

export default () => (
    <Layout title="Blog Page">
        <ul>
            <li><PostLik slug="react-post" title="React" /></li>
            <li><PostLik slug="angular-post" title="Angular" /></li>
            <li><PostLik slug="vue-post" title="Vue" /></li>
        </ul>
    </Layout>
);