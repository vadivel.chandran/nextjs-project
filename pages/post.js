import Layout from "../components/Layout";
import { withRouter } from "next/router"

const Post = ({ router }) => (
    <Layout title={router.query.title}>
        {(() => {
            if (router.query.title === 'React') {
                return (<p> React Content</p>)
            }
            else if (router.query.title === 'Angular') {
                return (<p> Angular Content</p>)
            }
            else if (router.query.title === 'Vue') {
                return (<p> Vue Content</p>)
            }
        })()}

    </Layout>

);
export default withRouter(Post);