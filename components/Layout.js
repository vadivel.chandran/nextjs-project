import Link from "next/link";
import Head from "next/head";
import Router from "next/router";
import Nprogress from "nprogress";

Router.onRouteChangeStart = url => {
  console.log(url);
  Nprogress.start();
};

Router.onRouterChangeComplete = () => Nprogress.done();
Router.onRouteChangeError = () => Nprogress.done();

const Layout = ({ children, title }) => (
  <div className="root">
    <Head>
      <title>ReactJS Protfoliio</title>
      
      
    </Head>
    <header>
      <Link href="/">
        <a>Home</a>
      </Link>
      <Link href="/about">
        <a>About</a>
      </Link>
      <Link href="/hireme">
        <a>HireMe</a>
      </Link>
    </header>
    <h1>{title}</h1>
    {children}
    <footer>@copyright 2020</footer>
   
    <style jsx>
      {`
        .root {
          display: flex;
          justify-content: center;
          align-items: center;
          flex-direction: column;
        }

        header {
          width: 100%;
          display: flex;
          justify-content: space-around;
          padding: 1em;
          font-size: 1.2rem;
          background: blue;
        }

        header a {
          color: darkgrey;
          text-decoration: none;
        }

        header a:hover {
          color: lightgrey;
          font-weight: bold;
        }
        footer {
          padding: 1em;
        }
      `}
    </style>

    <style global jsx>
      {`
        body {
          margin: 0;
          font-size: 110%;
          background: #f0f0f0;
        }
      `}
    </style>
  </div>
);

export default Layout;
